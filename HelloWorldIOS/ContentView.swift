//
//  ContentView.swift
//  HelloWorldIOS
//
//  Created by Joe on 7/21/19.
//  Copyright © 2019 QvaDev. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello World--1ST CHANGE--2nd CHANGE--3RD CHANGE-CHANGE-4ST ")
    }
}

#if DEBUG
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
